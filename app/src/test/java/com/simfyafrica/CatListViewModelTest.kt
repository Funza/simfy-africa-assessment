package com.simfyafrica

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.simfyafrica.helpers.DataConverter
import com.simfyafrica.helpers.DataState
import com.simfyafrica.helpers.NetworkHelper
import com.simfyafrica.helpers.ResourceDataStateFlow
import com.simfyafrica.model.CatList
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.model.pojo.CatResult
import com.simfyafrica.repository.CatRoomRepository
import com.simfyafrica.repository.cat.CatRepository
import com.simfyafrica.ui.catlist.CatListViewModel
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import retrofit2.Response.error


class CatListViewModelTest {
    private var viewModel: CatListViewModel? = null

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var dataConverter: DataConverter

    @Mock
    private lateinit var networkHelper: NetworkHelper

    @Mock
    private lateinit var catRepository: CatRepository

    @Mock
    private lateinit var catRoomRepository: CatRoomRepository

    @Mock
    private lateinit var application: AppApplication

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        dataConverter = DataConverter()
        viewModel = CatListViewModel(catRoomRepository, catRepository, networkHelper, application)
    }

    @Test
    fun getAllCatsSuccessfully() = runBlocking {
        val catList = CatList()
        val dataConverter = DataConverter()
        val mappedCatData = mappedCatData(catsData().body()!!)
        catList.cats = dataConverter.fromDailyList(mappedCatData)

        `when`(catRepository.getAllCats()).thenReturn(catsData())

        viewModel?.subscribeToAvailableCats()

        `when`(catRoomRepository.insertCat(catList)).thenReturn(null)

        val value: ResourceDataStateFlow<List<MappedCatInfo>?>? = viewModel?.getCatLiveData()?.value

        Assert.assertNotNull(value)
        Assert.assertEquals(DataState.SUCCESS, value?.state)
    }

    @Test
    fun getAllCatsFailure() = runBlocking {

        val errorResponse: retrofit2.Response<List<CatResult>> = error(
            403,
            ResponseBody.create(MediaType.parse("application/json"), "{\"key\":[\"somestuff\"]}")
        )

        `when`(catRepository.getAllCats()).thenReturn(errorResponse)

        viewModel?.subscribeToAvailableCats()

        val value: ResourceDataStateFlow<List<MappedCatInfo>?>? = viewModel?.getCatLiveData()?.value

        Assert.assertNotNull(value)
        Assert.assertEquals(DataState.ERROR, value?.state)
    }

    private fun catsData(): retrofit2.Response<List<CatResult>> {
        val catResultList = ArrayList<CatResult>()
        val catResult1 = CatResult()
        catResult1.id = "Kitty1"
        catResult1.sourceUrl = "https//www.cat.mark.co.za"
        catResult1.url = "https//www.cat.co.za"

        val catResult2 = CatResult()
        catResult2.id = "Kitty2"
        catResult2.sourceUrl = "https//www.cat.mark.co.za"
        catResult2.url = "https//www.cat.co.za"

        val catResult3 = CatResult()
        catResult3.id = "Kitty3"
        catResult3.sourceUrl = "https//www.cat.mark.co.za"
        catResult3.url = "https//www.cat.co.za"

        catResultList.add(catResult1)
        catResultList.add(catResult2)
        catResultList.add(catResult3)

        return retrofit2.Response.success(catResultList)
    }

    private fun mappedCatData(catResult: List<CatResult>): List<MappedCatInfo> {
        val mappedCatInfo = ArrayList<MappedCatInfo>()
        val mappedCatInfo1 =
            MappedCatInfo(
                catResult[0].id.toString(),
                catResult[0].sourceUrl.toString(),
                catResult[0].url
            )
        val mappedCatInfo2 =
            MappedCatInfo(
                catResult[1].id.toString(),
                catResult[1].sourceUrl.toString(),
                catResult[1].url
            )
        val mappedCatInfo3 =
            MappedCatInfo(
                catResult[2].id.toString(),
                catResult[2].sourceUrl.toString(),
                catResult[2].url
            )

        mappedCatInfo.add(mappedCatInfo1)
        mappedCatInfo.add(mappedCatInfo2)
        mappedCatInfo.add(mappedCatInfo3)

        return mappedCatInfo
    }

}