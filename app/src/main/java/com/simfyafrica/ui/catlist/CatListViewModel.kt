package com.simfyafrica.ui.catlist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.simfyafrica.R
import com.simfyafrica.helpers.DataConverter
import com.simfyafrica.helpers.NetworkHelper
import com.simfyafrica.helpers.ResourceDataStateFlow
import com.simfyafrica.model.CatList
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.model.pojo.CatResult
import com.simfyafrica.repository.CatRoomRepository
import com.simfyafrica.repository.cat.CatRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CatListViewModel @Inject constructor(
    private val localRepo: CatRoomRepository,
    private val catRepository: CatRepository,
    networkHelper: NetworkHelper, application: Application
) : AndroidViewModel(application) {

    private var catDescription = ""

    private var dataConverter = DataConverter()

    private var _catLiveData: MutableLiveData<ResourceDataStateFlow<List<MappedCatInfo>?>> =
        MutableLiveData()

    private var _cacheError: MutableLiveData<String> =
        MutableLiveData()

    private var _dialogError: MutableLiveData<Any> =
        MutableLiveData()

    fun getCatLiveData(): LiveData<ResourceDataStateFlow<List<MappedCatInfo>?>> = _catLiveData
    fun getDialogError(): LiveData<Any> = _dialogError
    fun getCacheErrorMessage(): LiveData<String> = _cacheError

    init {
        catDescription = application.getString(R.string.cats_description)
        if (networkHelper.isNetworkAvailable(application.baseContext)) {
            subscribeToCachedData(true)
        } else {
            subscribeToCachedData(false)
        }
    }

    fun subscribeToAvailableCats() {
        _catLiveData.value = ResourceDataStateFlow.postLoading()
        viewModelScope.launch {
            val result = catRepository.getAllCats()
            if (result.isSuccessful) {
                Timber.d("Success fully Loader ${result.body()?.get(0)?.id}")
                result.body()?.let { handleDataResults(it) }
            } else {
                Timber.d("Error fully Loader")
                _catLiveData.postValue(
                    ResourceDataStateFlow.postError(
                        result.errorBody().toString()
                    )
                )
            }
        }
    }

    private fun handleDataResults(result: List<CatResult>) {
        val catList = CatList()
        val dataConverter = DataConverter()
        val mappedCatData = mapData(result)
        catList.cats = dataConverter.fromList(mappedCatData)
        persistDataLocally(catList)
        _catLiveData.postValue(ResourceDataStateFlow.postSuccess(mappedCatData))
    }

    private fun mapData(cats: List<CatResult>): List<MappedCatInfo> {
        val data: MutableList<MappedCatInfo> = ArrayList()
        cats.indices.forEach { i ->
            val catName = "Kitty$i "
            val mappedCatInfo = MappedCatInfo(
                catName,
                catName + catDescription, cats[i].url
            )
            data.add(mappedCatInfo)
        }
        return data
    }

    private fun persistDataLocally(catList: CatList) {
        viewModelScope.launch {
            try {
                catList.let { localRepo.insertCat(it) }
            } catch (ex: Exception) {
                _cacheError.value = ex.message
            }

        }
    }

    private fun subscribeToCachedData(isInternetAvailable: Boolean) {
        _catLiveData.value = ResourceDataStateFlow.postLoading()
        viewModelScope.launch {
            try {
                val results = localRepo.getAllCats()
                if (results.isNotEmpty()) {
                    _catLiveData.postValue(
                        ResourceDataStateFlow.postSuccess(
                            dataConverter.toList(
                                results[0].cats
                            )
                        )
                    )
                } else {
                    if (isInternetAvailable) {
                        subscribeToAvailableCats()
                    } else {
                        _dialogError.postValue(Any())
                    }
                }
            } catch (ex: Exception) {
                _catLiveData.postValue(
                    ResourceDataStateFlow.postError(
                        ex.message.toString()
                    )
                )
            }

        }
    }
}