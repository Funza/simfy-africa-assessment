package com.simfyafrica.ui.catlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.simfyafrica.R
import com.simfyafrica.databinding.CatListFragmentBinding
import com.simfyafrica.helpers.Constants.SELECTED_CAT_BUNDLE
import com.simfyafrica.helpers.DataState
import com.simfyafrica.helpers.FragmentViewBound
import com.simfyafrica.helpers.ResourceDataStateFlow
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.navigator.Navigator
import com.simfyafrica.ui.adapter.CatsAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CatsListFragment : Fragment() {

    private var _binding: CatListFragmentBinding by FragmentViewBound()

    private val viewModel: CatListViewModel by viewModels()

    @Inject
    lateinit var navigator: Navigator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = CatListFragmentBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        viewModel.apply {
            getCatLiveData().observe(viewLifecycleOwner) {
                if (it != null) {
                    subscribeToResults(it)
                }
            }

            getDialogError().observe(viewLifecycleOwner) {
                showErrorDialog(getString(R.string.network_error_message))
            }

            getCacheErrorMessage().observe(viewLifecycleOwner) {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun subscribeToResults(results: ResourceDataStateFlow<List<MappedCatInfo>?>) {
        when (results.state) {
            DataState.ERROR -> {
                showErrorDialog(results.error)
                _binding.progressBar.hide()
            }
            DataState.LOADING -> {
                _binding.progressBar.show()
            }
            DataState.SUCCESS -> {
                val catsAdapter = CatsAdapter(::onCatSelected)
                _binding.catsList.adapter = catsAdapter
                catsAdapter.setData(results.data)
                _binding.progressBar.hide()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    private fun onCatSelected(catResult: MappedCatInfo) {
        navigator.navigate(
            R.id.action_SecondFragment_to_catDetailsFragment,
            bundleOf(SELECTED_CAT_BUNDLE to catResult)
        )
    }

    private fun showErrorDialog(message: String) {
        val dlg: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        dlg.setTitle(getString(R.string.networ_error_header))
        dlg.setMessage(message)
        dlg.setPositiveButton(
            getString(R.string.close)
        ) { _, _ ->
            activity?.finish()
        }.show()
    }
}