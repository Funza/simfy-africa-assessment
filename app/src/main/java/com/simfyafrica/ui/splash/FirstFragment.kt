package com.simfyafrica.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.simfyafrica.R
import com.simfyafrica.databinding.FragmentFirstBinding
import com.simfyafrica.helpers.FragmentViewBound
import com.simfyafrica.navigator.Navigator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

@AndroidEntryPoint
class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding by FragmentViewBound()

    @Inject
    lateinit var navigator: Navigator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFirstBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity?)?.supportActionBar?.hide()

        val animation: Animation = AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.nav_default_pop_exit_anim
        )
        animation.interpolator = LinearInterpolator()
        animation.repeatCount = Animation.INFINITE
        animation.duration = 800

        _binding.logo.startAnimation(animation)

        lifecycleScope.launch {
            delay(3050)
            try {
                navigator.navigate(R.id.action_FirstFragment_to_SecondFragment)
            } catch (e: IOException) {
                Timber.d(e.message.toString())
            }
        }
    }
}