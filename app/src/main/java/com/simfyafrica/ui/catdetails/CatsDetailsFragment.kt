package com.simfyafrica.ui.catdetails

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.simfyafrica.R
import com.simfyafrica.databinding.CatDetailsFragmentBinding
import com.simfyafrica.helpers.Constants.SELECTED_CAT_BUNDLE
import com.simfyafrica.helpers.FragmentViewBound
import com.simfyafrica.helpers.downloadImage
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.navigator.Navigator
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CatsDetailsFragment : Fragment(), Player.EventListener {

    private var _binding: CatDetailsFragmentBinding by FragmentViewBound()

    private lateinit var simpleExoplayer: SimpleExoPlayer

    private val dataSourceFactory: DataSource.Factory by lazy {
        DefaultDataSourceFactory(requireContext(), "exoplayer-sample")
    }

    @Inject
    lateinit var navigator: Navigator

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = CatDetailsFragmentBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        _binding.playIcon.setOnClickListener {
            playSound()
        }

        val selectedCatArg = arguments?.getSerializable(SELECTED_CAT_BUNDLE) as? MappedCatInfo?
        _binding.selectedCatDescription.text = selectedCatArg?.description
        _binding.selectedCatName.text = selectedCatArg?.tile

        _binding.selectedImg.downloadImage(selectedCatArg?.imageUrl)

        simpleExoplayer = SimpleExoPlayer.Builder(requireContext()).build()
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    private fun releasePlayer() {
        simpleExoplayer.release()
    }

    private fun preparePlayer(audioUri: String) {
        val uri = Uri.parse(audioUri)
        val mediaSource = buildMediaSource(uri)
        simpleExoplayer.prepare(mediaSource)
    }

    private fun buildMediaSource(uri: Uri): MediaSource {
        return ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(uri)
    }

    private fun playSound() {
        preparePlayer(getString(R.string.cat_sound_url))
        simpleExoplayer.playWhenReady = true
        simpleExoplayer.addListener(this)
    }

    override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
        when (playbackState) {
            Player.STATE_READY,
            Player.STATE_BUFFERING -> {
                _binding.playIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.exo_controls_pause
                    )
                )
            }
            Player.STATE_IDLE,
            Player.STATE_ENDED -> {
                _binding.playIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        requireContext(),
                        R.drawable.exo_controls_play
                    )
                )
            }
        }
    }
}