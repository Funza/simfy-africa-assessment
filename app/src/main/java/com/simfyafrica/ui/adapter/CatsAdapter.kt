package com.simfyafrica.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.simfyafrica.R
import com.simfyafrica.databinding.ActivityCatsItemBinding
import com.simfyafrica.helpers.downloadImage
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.model.pojo.CatResult
import kotlinx.android.synthetic.main.activity_cats_item.view.*
import kotlin.reflect.KFunction1

class CatsAdapter(private val onItemClicked: KFunction1<MappedCatInfo, Unit>) :
    RecyclerView.Adapter<CatsAdapter.ViewHolder>() {

    lateinit var binding: ActivityCatsItemBinding

    private val catsList: ArrayList<MappedCatInfo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding =
            ActivityCatsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    fun setData(data: List<MappedCatInfo>?) {
        catsList.clear()
        data?.let { catsList.addAll(it) }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(catsList[position], onItemClicked)
    }

    class ViewHolder(binding: ActivityCatsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindItems(item: MappedCatInfo, onItemClicked: (MappedCatInfo) -> Unit) {
            val imgUrl: String? = item.imageUrl

            itemView.rootView.cat_img.downloadImage(imgUrl)

            itemView.rootView.cat_value.text = item.tile

            itemView.setOnClickListener { onItemClicked(item) }

        }
    }

    override fun getItemCount(): Int = catsList.size
}