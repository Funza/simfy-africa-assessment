package com.simfyafrica.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.simfyafrica.model.CatList

@Dao
interface MainDao {

    @Insert(onConflict = REPLACE)
    suspend fun insertCat(cat: CatList)


    @Query("SELECT * FROM catList")
   suspend fun getAllCats(): List<CatList>
}