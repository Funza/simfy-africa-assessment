package com.simfyafrica.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.simfyafrica.model.CatList

@Database(entities = [CatList::class], version = 2, exportSchema = false)
abstract class MainDataBase : RoomDatabase() {
    abstract fun mainDao(): MainDao
}