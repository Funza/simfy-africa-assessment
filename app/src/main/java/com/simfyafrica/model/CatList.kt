package com.simfyafrica.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.simfyafrica.helpers.DataConverter

@Entity(tableName = "catList")
class CatList {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id: Int? = null

    @TypeConverters(DataConverter::class)
    @ColumnInfo(name = "cats")
    var cats: String? = null


}