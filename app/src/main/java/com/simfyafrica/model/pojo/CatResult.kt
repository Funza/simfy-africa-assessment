package com.simfyafrica.model.pojo

import com.google.gson.annotations.SerializedName

class CatResult {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("sourceUrl")
    var sourceUrl: String? = null
}