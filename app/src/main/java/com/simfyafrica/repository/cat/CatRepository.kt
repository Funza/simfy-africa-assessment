package com.simfyafrica.repository.cat

import com.simfyafrica.repository.cat.ApiImpl
import javax.inject.Inject

class CatRepository @Inject constructor(private val apiImpl: ApiImpl) {
    suspend fun getAllCats() = apiImpl.getAllCats()
}