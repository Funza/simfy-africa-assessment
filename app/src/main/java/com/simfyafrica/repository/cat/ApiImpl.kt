package com.simfyafrica.repository.cat

import com.simfyafrica.model.pojo.CatResult
import retrofit2.Response

interface ApiImpl {
    suspend fun getAllCats(): Response<List<CatResult>>
}