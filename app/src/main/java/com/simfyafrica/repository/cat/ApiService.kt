package com.simfyafrica.repository.cat

import com.simfyafrica.model.pojo.CatResult
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("get?format=json&results_per_page=100&size=small&type=png?")
    suspend fun getAllAvailableCats(): Response<List<CatResult>>
}