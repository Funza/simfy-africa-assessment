package com.simfyafrica.repository.cat

import com.simfyafrica.model.pojo.CatResult
import retrofit2.Response
import javax.inject.Inject

class CatRepositoryImpl @Inject constructor(private val apiService: ApiService) : ApiImpl {
    override suspend fun getAllCats(): Response<List<CatResult>> = apiService.getAllAvailableCats()
}
