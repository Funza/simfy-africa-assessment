package com.simfyafrica.repository

import com.simfyafrica.db.MainDao
import com.simfyafrica.model.CatList
import com.simfyafrica.model.pojo.CatResult
import javax.inject.Inject

class CatRoomRepository @Inject constructor(val mainDao: MainDao) {

    suspend fun insertCat(cat: CatList) = mainDao.insertCat(cat)

    suspend fun getAllCats() = mainDao.getAllCats()


}