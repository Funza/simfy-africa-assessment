package com.simfyafrica.helpers

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.simfyafrica.model.MappedCatInfo
import com.simfyafrica.model.pojo.CatResult
import java.lang.reflect.Type

class DataConverter {

    @TypeConverter
    fun fromList(cats: List<MappedCatInfo?>?): String? {
        if (cats == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<MappedCatInfo?>?>() {}.type
        return gson.toJson(cats, type)
    }

    @TypeConverter
    fun toList(toCatsString: String?): List<MappedCatInfo>? {
        if (toCatsString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<MappedCatInfo?>?>() {}.type
        return gson.fromJson<List<MappedCatInfo>>(toCatsString, type)
    }

}