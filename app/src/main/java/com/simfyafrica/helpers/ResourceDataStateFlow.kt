package com.simfyafrica.helpers

class ResourceDataStateFlow<T> {
    var state: DataState
    var data: T? = null
    lateinit var error: String

    constructor(success: DataState, data: T) {
        this.state = success
        this.data = data
        this.data = data
    }

    constructor(error: String) {
        this.state = DataState.ERROR
        this.error = error
    }

    constructor() {
        this.state = DataState.LOADING
    }

    companion object {
        fun <T> postSuccess(data: T): ResourceDataStateFlow<T> {
            return ResourceDataStateFlow(
                DataState.SUCCESS,
                data
            )
        }

        fun <T> postError(throwable: String): ResourceDataStateFlow<T> {
            return ResourceDataStateFlow(throwable)
        }

        fun <T> postLoading(): ResourceDataStateFlow<T> {
            return ResourceDataStateFlow()
        }
    }
}