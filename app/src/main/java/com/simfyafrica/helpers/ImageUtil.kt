package com.simfyafrica.helpers

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.simfyafrica.R

fun ImageView.downloadImage(imageUrl: String?) {
    val options: RequestOptions = RequestOptions().centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).error(R.drawable.placeholder)
    Glide.with(this).load(imageUrl).apply(options).into(this)
}