package com.simfyafrica.helpers

object Constants {

    const val MAIN_DATABASE_NAME = "cat_db"
    const val BASE_URL = "https://api.thecatapi.com/api/images/"
    const val SELECTED_CAT_BUNDLE = "selectedCat"
}