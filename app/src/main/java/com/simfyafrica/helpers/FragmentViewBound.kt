package com.simfyafrica.helpers

import androidx.fragment.app.Fragment
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import kotlin.reflect.KProperty

class FragmentViewBound<T> {

    internal var value: T? = null

    operator fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        return value ?: throw IllegalStateException("Fragment doesn't have a view: $thisRef")
    }

    operator fun setValue(thisRef: Fragment, property: KProperty<*>, newValue: T) {
        if (value == null) {

            thisRef.viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    value = null
                }
            })
        }
        value = newValue
    }
}