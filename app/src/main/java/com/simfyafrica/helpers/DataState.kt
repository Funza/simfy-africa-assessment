package com.simfyafrica.helpers

enum class DataState {
    SUCCESS, ERROR, LOADING
}