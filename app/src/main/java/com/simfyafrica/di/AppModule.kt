package com.simfyafrica.di

import android.content.Context
import androidx.room.Room
import com.simfyafrica.db.MainDataBase
import com.simfyafrica.helpers.Constants.BASE_URL
import com.simfyafrica.helpers.Constants.MAIN_DATABASE_NAME
import com.simfyafrica.repository.cat.CatRepositoryImpl
import com.simfyafrica.repository.cat.ApiImpl
import com.simfyafrica.repository.cat.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRunningDatabase(@ApplicationContext app: Context) =
        Room.databaseBuilder(app, MainDataBase::class.java, MAIN_DATABASE_NAME).fallbackToDestructiveMigration().build()

    @Singleton
    @Provides
    fun provideMainDao(db: MainDataBase) = db.mainDao()

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Singleton
    @Provides
    fun provideApiImpl(apiImpl: CatRepositoryImpl): ApiImpl = apiImpl
}